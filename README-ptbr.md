<p align="center">
  <img src="/src/assets/logo.svg" width="150" />
</p>
<p align="center">⭐⭐⭐⭐⭐</p>
<h1 align="center">Jogo da Cobrinha - Flúido e Responsivo</h1>
<h4 align="center"><a href="https://alexrintt.github.io/snake-game"><code>Jogue no Browser</code></a></h4>
<p align="center">Uma implementação do clássico jogo Snake com o HTML5 Canvas Engine. Escrito em Typescript</p>
<p align="center">
  <img  src="https://img.shields.io/badge/category-gamedev-orange" alt="Application Category" />
  <img  src="https://img.shields.io/badge/language-typescript-blue" alt="Repo Main Language" />
  <img  src="https://img.shields.io/badge/bundler-parcel-blueviolet" alt="Module Bundler" />
  <img  src="https://img.shields.io/badge/architecture-factory_functions-blueviolet" alt="Repo API" />
  <img  src="https://img.shields.io/badge/type-project-success" alt="Repo Type" />
  <img  src="https://img.shields.io/badge/name-snake_game-green" alt="Game Ref" />
</p>

<p align="center">
  <a href="https://www.linkedin.com/in/alexrintt" target="_blank">
    <img src="https://img.shields.io/twitter/url?label=Connect%20%40alexrintt&logo=linkedin&url=https%3A%2F%2Fwww.twitter.com%2Falexrintt%2F" alt="Follow" />
  </a>
</p>

<p align="center">
  <img src="./src/assets/snake-game-printscreen.png" width="350">
</p>

<p>
  <img src="./src/assets/en.png" alt="Portuguese" height="16">
  <a href="https://github.com/alexrintt/snake-game/">Read in English</a>
</p>

# Jogue no Browser

[Clicando aqui](https://alexrintt.github.io/snake-game/) você pode jogar, divirta-se :D

# O que é este repositório?

É uma implementação do clássico jogo Snake com HTML5 Canvas e Typescript. Diferenciais: é responsivo; portanto, é possível abrir no dispositivo móvel ou em um desktop para a mesma experiência. No celular, os comandos Snake são manipulados com gestos de swipe, deslizar, aproveite

# Como clonar este projeto?

1. Clone o repositório

```
git clone https://github.com/alexrintt/snake-game.git
```

2. Mude o diretório

```
cd snake-game
```

3. Instale as dependências (2 dependências: hammerjs para trabalhar com o gestos e o lodash.debounce pra melhorar o desempenho)

```
yarn install
```

ou, para usuários de npm:

```
npm install
```

4. Inicie o servidor de desenvolvimento

```
yarn dev
```

ou, para usuários de npm:

```
npm run dev
```

5. Gerar os arquivos estáticos e fazer deploy para o github pages (gh-pages branch)

```
yarn build
```

ou, para usuários de npm:

```
npm run build
```

<br>
<br>
<br>

<samp>

<h2 align="center">
  Open Source
</h2>
<p align="center">
  <sub>Copyright © 2020-present, Alex Rintt.</sub>
</p>
<p align="center">Web Snake Game Code <a href="https://github.com/alexrintt/ethereal-color/LICENSE.md">is MIT licensed 💖</a></p>
<p align="center">
  <img src="./src/assets/logo64.png" width="35" />
</p>

</samp>
