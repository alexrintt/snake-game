<p align="center">
  <img src="/src/assets/logo.svg" width="150" />
</p>
<p align="center">⭐⭐⭐⭐⭐</p>
<h1 align="center">Web Snake Game - Fluid and Responsive</h1>
<h4 align="center"><a href="https://alexrintt.github.io/snake-game"><code>Play in Browser</code></a></h4>
<p align="center">A implementation of classic Snake Game with HTML5 Canvas Engine. Written in Typescript</p>
<p align="center">
  <img  src="https://img.shields.io/badge/category-gamedev-orange" alt="Application Category" />
  <img  src="https://img.shields.io/badge/language-typescript-blue" alt="Repo Main Language" />
  <img  src="https://img.shields.io/badge/bundler-parcel-blueviolet" alt="Module Bundler" />
  <img  src="https://img.shields.io/badge/architecture-factory_functions-blueviolet" alt="Repo API" />
  <img  src="https://img.shields.io/badge/type-project-success" alt="Repo Type" />
  <img  src="https://img.shields.io/badge/name-snake_game-green" alt="Game Ref" />
</p>

<p align="center">
  <a href="https://www.linkedin.com/in/alexrintt" target="_blank">
    <img src="https://img.shields.io/twitter/url?label=Connect%20%40alexrintt&logo=linkedin&url=https%3A%2F%2Fwww.twitter.com%2Falexrintt%2F" alt="Follow" />
  </a>
</p>

<p align="center">
  <img src="./src/assets/snake-game-printscreen.png" width="350">
</p>

<p>
  <img src="./src/assets/pt-br.png" alt="Portuguese" height="16">
  <a href="https://github.com/alexrintt/snake-game/blob/master/README-ptbr.md">Ler em português</a>
</p>

## Play in Browser

[Clicking here](https://alexrintt.github.io/snake-game/) you can play, enjoy :D

## What is it?

Its a implementation of classic Snake Game with HTML5 Canvas and Typescript. Differentials: is responsive, therefore is possible to open in mobile device or a desktop for same experience. In mobile, the Snake commands are handled with swipe gestures, enjoy

## Support

If you have ideas to share, bugs to report or need support, you can either open an issue or join our Discord server

<a href="https://discord.gg/86GDERXZNS">
  <kbd><img src="https://discordapp.com/api/guilds/771498135188799500/widget.png?style=banner2" alt="Discord Banner"/></kbd>
</a>

## How to clone project

### 1. Clone repository

```
git clone https://github.com/alexrintt/snake-game.git
```

### 2. Change directory

```
cd snake-game
```

### 3. To install dependencies (2 dependencies: hammerjs to work with gestures and lodash.debounce to improve performance)

```
yarn install
```

or, for npm users:

```
npm install
```

### 4. To run development server

```
yarn dev
```

or, for npm users:

```
npm run dev
```

### 5. To create static files and deploy to github pages (gh-pages branch)

```
yarn build
```

or, for npm users:

```
npm run build
```

<br>
<br>
<br>

<samp>

<h2 align="center">
  Open Source
</h2>
<p align="center">
  <sub>Copyright © 2020-present, Alex Rintt.</sub>
</p>
<p align="center">Web Snake Game Code <a href="https://github.com/alexrintt/snake-game/blob/master/LICENSE.md">is MIT licensed 💖</a></p>
<p align="center">
  <img src="./src/assets/logo64.png" width="35" />
</p>

</samp>
